import React, { useState, useEffect } from 'react';

// Function to generate random data for lazy loading example
const generateRandomData = (numItems) => {
  // Implement the function to generate random data (e.g., an array of objects)
  // For this example, let's return an array of random strings
  const randomData = [];
  for (let i = 1; i <= numItems; i++) {
    randomData.push(`Item ${i}`);
  }
  return randomData;
};

const LazyLoadingList = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 10;

  useEffect(() => {
    // Simulate API call to fetch initial data (using a random generator function here)
    const fetchData = () => {
      setLoading(true);
      const randomData = generateRandomData(50); // Generating 50 random items for demonstration
      setData(randomData.slice(0, itemsPerPage));
      setLoading(false);
    };

    fetchData();
  }, []);

  const handleLazyLoad = () => {
    // Simulate loading more data when the user reaches the end of the page
    const nextPage = currentPage + 1;
    const startIndex = (nextPage - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;

    setLoading(true);
    setTimeout(() => {
      setData((prevData) => [...prevData, ...generateRandomData(50).slice(startIndex, endIndex)]);
      setLoading(false);
      setCurrentPage(nextPage);
    }, 1000); // Simulating a delay to fetch more data (replace with actual API call)
  };

  return (
    <div>
      <h2>List with Lazy Loading</h2>
      <ul>
        {data.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
      </ul>
      {loading ? <p>Loading...</p> : null}
      {!loading && data.length < 50 && (
        <button onClick={handleLazyLoad}>Load More</button>
      )}
    </div>
  );
};

export default LazyLoadingList;
