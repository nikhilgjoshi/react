import React, { useState, useEffect, useRef } from "react";

// Task 1: Creating the folder structure
const Folder = ({ folder, depth, onFileEdit, onFileDelete, onFolderCreate }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [newFileName, setNewFileName] = useState(folder.name);
  const inputRef = useRef(null);

  const handleEdit = () => {
    setIsEditing(true);
    setNewFileName(folder.name);
  };

  const handleCancelEdit = () => {
    setIsEditing(false);
  };

  const handleSaveEdit = () => {
    onFileEdit(folder.id, newFileName);
    setIsEditing(false);
  };

  const handleDelete = () => {
    onFileDelete(folder.id);
  };

  const handleCreateFolder = () => {
    const folderName = prompt("Enter the name of the new folder:");
    if (folderName) {
      onFolderCreate(folder.id, folderName);
    }
  };

  return (
    <div style={{ marginLeft: `${depth * 20}px` }}>
      {isEditing ? (
        <div>
          <input
            type="text"
            value={newFileName}
            onChange={(e) => setNewFileName(e.target.value)}
            ref={inputRef}
          />
          <button onClick={handleSaveEdit}>Save</button>
          <button onClick={handleCancelEdit}>Cancel</button>
        </div>
      ) : (
        <div>
          <span>{folder.name}</span>
          <button onClick={handleEdit}>Edit</button>
          <button onClick={handleDelete}>Delete</button>
          <button onClick={handleCreateFolder}>Create Folder</button>
        </div>
      )}
      {folder.children &&
        folder.children.map((child) => (
          <Folder
            key={child.id}
            folder={child}
            depth={depth + 1}
            onFileEdit={onFileEdit}
            onFileDelete={onFileDelete}
            onFolderCreate={onFolderCreate}
          />
        ))}
    </div>
  );
};

// Task 2: Lazy Loading To Avoid Pagination
const LazyLoader = () => {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const pageSize = 5;

  const loadData = async () => {
    try {
      const response = await fetch(
        `https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=${pageSize}`
      );
      const newData = await response.json();
      setData((prevData) => [...prevData, ...newData]);
      setPage((prevPage) => prevPage + 1);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const handleLoadMore = () => {
    loadData();
  };

  useEffect(() => {
    loadData();
  }, []);

  return (
    <div>
      <div style={{ height: "200px", overflow: "auto" }}>
        {data.map((item, index) => (
          <div key={index}>
            <h3>{item.title}</h3>
            <p>{item.body}</p>
          </div>
        ))}
      </div>
      <button onClick={handleLoadMore}>Load More</button>
    </div>
  );
};

const App = () => {
  // Sample JSON tree for Task 1
  const initialFolderStructure = {
    id: 1,
    name: "Root",
    children: [
      {
        id: 2,
        name: "Folder 1",
        children: [
          { id: 5, name: "File 1" },
          { id: 6, name: "File 2" },
        ],
      },
      {
        id: 3,
        name: "Folder 2",
        children: [
          { id: 7, name: "File 3" },
          { id: 8, name: "File 4" },
        ],
      },
      { id: 4, name: "File 5" },
    ],
  };

  const [folderStructure, setFolderStructure] = useState(initialFolderStructure);

  const handleFileEdit = (fileId, newName) => {
    const updateFolderStructure = (folder) => {
      if (folder.id === fileId) {
        return { ...folder, name: newName };
      } else if (folder.children) {
        return {
          ...folder,
          children: folder.children.map((child) => updateFolderStructure(child)),
        };
      }
      return folder;
    };
    setFolderStructure(updateFolderStructure(folderStructure));
  };

  const handleFileDelete = (fileId) => {
    const deleteFromFolderStructure = (folder) => {
      if (folder.children) {
        folder.children = folder.children.filter((child) => child.id !== fileId);
        folder.children.forEach(deleteFromFolderStructure);
      }
      return folder;
    };
    setFolderStructure(deleteFromFolderStructure({ ...folderStructure }));
  };

  const handleFolderCreate = (parentId, folderName) => {
    const newFolder = {
      id: Date.now(), // You can generate a unique ID here
      name: folderName,
    };
    const addFolderToStructure = (folder) => {
      if (folder.id === parentId) {
        if (!folder.children) {
          folder.children = [];
        }
        folder.children.push(newFolder);
      } else if (folder.children) {
        return {
          ...folder,
          children: folder.children.map((child) => addFolderToStructure(child)),
        };
      }
      return folder;
    };
    setFolderStructure(addFolderToStructure(folderStructure));
  };

  return (
    <div>
      <h1>Task 1: Folder Structure</h1>
      <Folder
        folder={folderStructure}
        depth={0}
        onFileEdit={handleFileEdit}
        onFileDelete={handleFileDelete}
        onFolderCreate={handleFolderCreate}
      />

      <h1>Task 2: Lazy Loading</h1>
      <LazyLoader />
    </div>
  );
};

export default App;